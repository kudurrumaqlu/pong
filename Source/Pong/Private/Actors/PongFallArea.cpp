#include "Actors/PongFallArea.h"
#include "Actors/PongBall.h"
#include "Game/PongArenaGameMode.h"
#include "Components/BoxComponent.h"
#include "Kismet/GameplayStatics.h"

APongFallArea::APongFallArea(const FObjectInitializer& ObjectInitializer)
	: Super(ObjectInitializer),
	PlayerIndex(0)
{
	Collision = CreateDefaultSubobject<UBoxComponent>(TEXT("Collision"));
}

void APongFallArea::NotifyActorBeginOverlap(AActor* OtherActor)
{
	if (HasAuthority() && OtherActor->IsA(APongBall::StaticClass()))
	{
		AGameModeBase* GameMode = UGameplayStatics::GetGameMode(GetWorld());
		APongArenaGameMode* PongGameMode = Cast<APongArenaGameMode>(GameMode);
		check(PongGameMode);
		if (PongGameMode)
		{
			PongGameMode->HandleFall(OtherActor, PlayerIndex);
		}
	}
}

#include "Actors/PongBall.h"
#include "Components/BoxComponent.h"
#include "Kismet/GameplayStatics.h"
#include "Player/PongPlayer.h"

APongBall::APongBall(const FObjectInitializer& ObjectInitializer)
	: Super(ObjectInitializer),
	InitialVelocity(200.f),
	PerHitVelocityIncrease(50.f),
	MaxPadReflectionAngle(45.f),
	HitCounter(0)
{
	Mesh = CreateDefaultSubobject<UStaticMeshComponent>(TEXT("Mesh"));
	Mesh->SetSimulatePhysics(true);
	Mesh->SetLinearDamping(0.f);
	Mesh->SetEnableGravity(false);
	Mesh->SetConstraintMode(EDOFMode::XYPlane);
	Mesh->SetNotifyRigidBodyCollision(true);
	RootComponent = Mesh;
}

void APongBall::NotifyHit(UPrimitiveComponent* MyComp, AActor* Other, UPrimitiveComponent* OtherComp, bool bSelfMoved,
	FVector HitLocation, FVector HitNormal, FVector NormalImpulse, const FHitResult& Hit)
{
	APongPlayer* Player = Cast<APongPlayer>(Other);
	if (Player)
	{
		// Velocity increases on every hit
		HitCounter++;
		const float NewScalarVelocity = InitialVelocity + HitCounter * PerHitVelocityIncrease;

		// Arkanoid-style reflection physics
		const float PlayerHalfWidth = Player->Collision->Bounds.BoxExtent.Y;
		// ImpactAlpha equals to 0 on pad's center and 1 on the edge
		float ImpactAlpha = FMath::Abs(Hit.ImpactPoint.Y - Player->GetActorLocation().Y) / PlayerHalfWidth;
		ImpactAlpha *= FMath::Sign((Player->GetActorForwardVector() ^ (Hit.ImpactPoint - Player->GetActorLocation())).Z);

		const FVector NewDirection = Player->GetActorForwardVector()
			.RotateAngleAxis(MaxPadReflectionAngle * ImpactAlpha, FVector::UpVector);
		
		Mesh->SetPhysicsLinearVelocity(NewDirection * NewScalarVelocity);

		if (HasAuthority())
		{
			MulticastPlayHitEffect();
		}
	}
}

void APongBall::BeginPlay()
{
	Super::BeginPlay();

	if (HasAuthority())
	{
		SetReplicates(true);
		SetReplicateMovement(true);
	}
}

void APongBall::MulticastPlayHitEffect_Implementation()
{
	check(PadHitSound);
	UGameplayStatics::PlaySound2D(GetWorld(), PadHitSound);
}

#include "Game/PongGameState.h"
#include "UI/PongHUD.h"
#include "Blueprint/UserWidget.h"
#include "GameFramework/GameMode.h"
#include "GameFramework/PlayerState.h"
#include "Kismet/GameplayStatics.h"
#include "Net/UnrealNetwork.h"
#include "Player/PongPlayerController.h"
#include "Player/PongPlayerState.h"

void APongGameState::EndMatch(uint32 Winner)
{
	WinnerIndex = Winner;
    SetMatchState(MatchState::WaitingPostMatch);
}

void APongGameState::HandleMatchHasStarted()
{
	if (!GetWorld()->HasBegunPlay())
	{
		if (HasAuthority())
		{
			bReplicatedHasBegunPlay = true;
		}

		GetWorldSettings()->NotifyBeginPlay();
		GetWorldSettings()->NotifyMatchStarted();
	}

	if (HasAuthority())
	{
		for (APlayerState* PlayerState : PlayerArray)
		{
			APongPlayerState* PongPlayerState = Cast<APongPlayerState>(PlayerState);
			check(PongPlayerState);
			PongPlayerState->UpdateScore(0.f);
		}
	}

	APongHUD* HUD = APongPlayerController::GetLocalHUD(GetWorld());
	if (HUD)
	{
		HUD->HideMatchResult();
	}
}

void APongGameState::HandleMatchHasEnded()
{
	APongHUD* HUD = APongPlayerController::GetLocalHUD(GetWorld());
	if (HUD)
	{
		HUD->DisplayMatchResult(FText::Format(NSLOCTEXT("Pong", "WinText", "Player {0} wins!"), WinnerIndex + 1));
	}
}

void APongGameState::GetLifetimeReplicatedProps(TArray<FLifetimeProperty>& OutLifetimeProps) const
{
	Super::GetLifetimeReplicatedProps(OutLifetimeProps);

	DOREPLIFETIME(APongGameState, WinnerIndex);
}

void APongGameState::MulticastNotifyFall_Implementation()
{
	check(FallSound);
    UGameplayStatics::PlaySound2D(GetWorld(), FallSound);
}

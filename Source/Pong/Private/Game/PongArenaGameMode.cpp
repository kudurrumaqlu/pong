#include "Game/PongArenaGameMode.h"
#include "Game/PongWorldSettings.h"
#include "Engine/LevelScriptActor.h"
#include "Actors/PongBall.h"
#include "Game/PongGameState.h"
#include "Blueprint/UserWidget.h"
#include "GameFramework/GameMode.h"
#include "GameFramework/PlayerState.h"
#include "Player/PongPlayerState.h"

APongArenaGameMode::APongArenaGameMode(const FObjectInitializer& ObjectInitializer)
	: Super(ObjectInitializer),
	MaxScore(3),
	SpawnDelay(1.f),
	MaxInitialBallAngle(50.f),
	MatchRestartDelay(5.f),
	LastWinnerIndex(0)
{
}

void APongArenaGameMode::BeginPlay()
{
	Super::BeginPlay();

	ServerWaitingWidget->SetVisibility(ESlateVisibility::Hidden);
}

AActor* APongArenaGameMode::FindPlayerStart_Implementation(AController* Player, const FString& IncomingName)
{
	APongWorldSettings* WorldSettings = Cast<APongWorldSettings>(GetWorldSettings());
	check(WorldSettings);
	
	return GetNumPlayers() > 1 ? WorldSettings->Player2Spawn : WorldSettings->Player1Spawn;
}

void APongArenaGameMode::StartPlay()
{
	Super::StartPlay();

	check(ServerWaitingWidgetClass);
	ServerWaitingWidget = CreateWidget<UUserWidget>(GetWorld(), ServerWaitingWidgetClass);
	check(ServerWaitingWidget);
	ServerWaitingWidget->AddToViewport();
}

void APongArenaGameMode::HandleStartingNewPlayer_Implementation(APlayerController* NewPlayer)
{
	Super::HandleStartingNewPlayer_Implementation(NewPlayer);

	APongPlayerState* PongPlayerState = Cast<APongPlayerState>(NewPlayer->PlayerState);
	check(PongPlayerState);
	PongPlayerState->SetPlayerIndex(GetNumPlayers() - 1);
	
    if (GetNumPlayers() > 1)
    {
    	StartNewMatch();
    }
}

void APongArenaGameMode::EndMatch(uint8 WinnerIndex)
{
	APongGameState* PongGameState = Cast<APongGameState>(GameState);
	check(PongGameState);
	PongGameState->EndMatch(WinnerIndex);

	GetWorld()->GetTimerManager().SetTimer(
		RestartTimerHandle,
		FTimerDelegate::CreateUObject(this, &APongArenaGameMode::StartNewMatch),
		MatchRestartDelay,
		false
	); 
}

void APongArenaGameMode::StartNewMatch()
{
	APongGameState* PongGameState = Cast<APongGameState>(GameState);
	PongGameState->SetMatchState(MatchState::InProgress);
	
	InitiateBallSpawn();
}

void APongArenaGameMode::HandleFall(AActor* BallActor, uint8 PlayerIndex)
{
	BallActor->Destroy();
	LastWinnerIndex = PlayerIndex;

	if (GameState->PlayerArray.Num() > PlayerIndex)
	{
		APlayerState* PlayerState = GameState->PlayerArray[PlayerIndex];
		check(PlayerState);
		APongPlayerState* PongPlayerState = Cast<APongPlayerState>(PlayerState);
		check(PlayerState);

		const float NewScore = PlayerState->GetScore() + 1.f;
		PongPlayerState->UpdateScore(NewScore);

		if (NewScore >= MaxScore)
		{
			EndMatch(PlayerIndex);
		}
		else
		{
			InitiateBallSpawn();
		}
	}

	APongGameState* PongGameState = Cast<APongGameState>(GameState);
	check(PongGameState);
	PongGameState->MulticastNotifyFall();
}

void APongArenaGameMode::InitiateBallSpawn()
{
	GetWorld()->GetTimerManager().SetTimer(
		BallSpawnTimer,
		FTimerDelegate::CreateUObject(this, &APongArenaGameMode::SpawnBall),
		SpawnDelay,
		false
	);
}

void APongArenaGameMode::SpawnBall()
{
	checkf(BallClass, TEXT("Ball actor class is not specified in game mode settings"));

	APongWorldSettings* WorldSettings = Cast<APongWorldSettings>(GetWorldSettings());
	check(WorldSettings);
	const FTransform SpawnTransform = WorldSettings->BallSpawn->GetActorTransform();
	const FActorSpawnParameters SpawnParameters;
	APongBall* Ball = GetWorld()->SpawnActor<APongBall>(BallClass, SpawnTransform, SpawnParameters);
	checkf(Ball, TEXT("Failed to spawn a ball"));

	Ball->Mesh->SetPhysicsLinearVelocity(GetInitialDirection() * Ball->GetInitialVelocity());
}

FVector APongArenaGameMode::GetInitialDirection() const
{
	const FRotator Direction(0.f, FMath::RandRange(-MaxInitialBallAngle, MaxInitialBallAngle), 0.f);
	FVector DirectionVector = Direction.Vector();

	if (LastWinnerIndex == 0) // Rotate ball to loser's side
	{
		DirectionVector *= -1.f;
	}
	
	return DirectionVector;
}

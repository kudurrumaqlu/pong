#include "UI/PongHUD.h"
#include "UI/PongInfoWidget.h"
#include "UI/PongScoreWidget.h"
#include "Blueprint/UserWidget.h"

void APongHUD::UpdateScore(uint32 PlayerId, uint32 Score) const
{
	ScoreWidget->ScoreUpdated(PlayerId, Score);
}

void APongHUD::DisplayMatchResult(FText ResultMessage) const
{
	InfoWidget->SetVisibility(ESlateVisibility::Visible);
	InfoWidget->UpdateStatusText(ResultMessage);
}

void APongHUD::HideMatchResult() const
{
	InfoWidget->SetVisibility(ESlateVisibility::Hidden);
}

void APongHUD::BeginPlay()
{
	check(ScoreWidgetClass);
	check(InfoWidgetClass);
	
	Super::BeginPlay();

	ScoreWidget = CreateWidget<UPongScoreWidget>(GetOwningPlayerController(), ScoreWidgetClass);
	check(ScoreWidget);
	ScoreWidget->AddToViewport();

	InfoWidget = CreateWidget<UPongInfoWidget>(GetOwningPlayerController(), InfoWidgetClass);
	check(InfoWidget);
	InfoWidget->AddToViewport();
	InfoWidget->SetVisibility(ESlateVisibility::Hidden);
}

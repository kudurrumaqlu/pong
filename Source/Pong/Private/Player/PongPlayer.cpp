#include "Player/PongPlayer.h"
#include "Game/PongWorldSettings.h"
#include "Components/BoxComponent.h"

APongPlayer::APongPlayer(const FObjectInitializer& ObjectInitializer)
	: Super(ObjectInitializer)
{
	Collision = CreateDefaultSubobject<UBoxComponent>(TEXT("Collision"));
	RootComponent = Collision;

	VisualMesh = CreateDefaultSubobject<UStaticMeshComponent>(TEXT("VisualMesh"));
	VisualMesh->SetCollisionEnabled(ECollisionEnabled::NoCollision);
	VisualMesh->SetupAttachment(Collision);
	
	InputSensitivity = 5.f;
}

void APongPlayer::ServerAddVerticalInput_Implementation(float Value)
{
	FVector CurrentLocation = GetActorLocation();
	CurrentLocation.Y = Value;
	SetActorLocation(CurrentLocation);
}

bool APongPlayer::ServerAddVerticalInput_Validate(float Value)
{
	return Value >= MinYLocation && Value <= MaxYLocation;
}

void APongPlayer::AddVerticalInput(float Value)
{
	const float Delta = Value * InputSensitivity;
	FVector CurrentLocation = GetActorLocation();
	const float NewY = CurrentLocation.Y + Delta;
	CurrentLocation.Y = FMath::Clamp(NewY, MinYLocation, MaxYLocation);
	SetActorLocation(CurrentLocation);
	
	if (GetLocalRole() == ROLE_AutonomousProxy)
	{
		ServerAddVerticalInput(CurrentLocation.Y);
	}
}

void APongPlayer::SetupPlayerInputComponent(UInputComponent* PlayerInputComponent)
{
	PlayerInputComponent->BindAxis(TEXT("VerticalInput"), this, &APongPlayer::AddVerticalInput);
	
	Super::SetupPlayerInputComponent(PlayerInputComponent);
}

void APongPlayer::OnConstruction(const FTransform& Transform)
{
	APongWorldSettings* WorldSettings = Cast<APongWorldSettings>(GetWorldSettings());
	check(WorldSettings);
	if (WorldSettings)
	{
		const float PadHalfWidth = Collision->GetUnscaledBoxExtent().Y;
		const float HalfDelta = WorldSettings->LevelHeight / 2.f - PadHalfWidth;
		const FVector InitialLocation = GetActorLocation();
		MaxYLocation = InitialLocation.Y + HalfDelta;
		MinYLocation = InitialLocation.Y - HalfDelta;
	}

	// Update visual model to physical model size
	VisualMesh->SetWorldScale3D(Collision->GetUnscaledBoxExtent() / 50.f);
}

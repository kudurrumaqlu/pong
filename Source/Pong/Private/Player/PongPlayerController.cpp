#include "Player/PongPlayerController.h"
#include "Blueprint/UserWidget.h"
#include "Camera/CameraActor.h"
#include "Kismet/GameplayStatics.h"
#include "UI/PongHUD.h"

// Both players share the same camera on the level
void APongPlayerController::AutoManageActiveCameraTarget(AActor* SuggestedTarget)
{
	AActor* Camera = UGameplayStatics::GetActorOfClass(GetWorld(), ACameraActor::StaticClass());
	if (IsValid(Camera))
	{
		SetViewTarget(Camera);
	}
	else
	{
		Super::AutoManageActiveCameraTarget(SuggestedTarget);
	}
}

APongHUD* APongPlayerController::GetLocalHUD(const UWorld* InWorld)
{
	APlayerController* LocalController = GEngine->GetFirstLocalPlayerController(InWorld);
	if (LocalController)
	{
		return Cast<APongHUD>(LocalController->GetHUD());
	}

	return nullptr;
}


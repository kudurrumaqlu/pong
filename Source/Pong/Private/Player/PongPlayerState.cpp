#include "Player/PongPlayerState.h"
#include "UI/PongHUD.h"
#include "Net/UnrealNetwork.h"
#include "Player/PongPlayerController.h"

APongPlayerState::APongPlayerState(const FObjectInitializer& ObjectInitializer)
	: Super(ObjectInitializer),
	PlayerIndex(INDEX_NONE)
{
}

void APongPlayerState::OnRep_Score()
{
	APongHUD* HUD = APongPlayerController::GetLocalHUD(GetWorld());
	if (HUD)
	{
		HUD->UpdateScore(PlayerIndex, GetScore());
	}
}

void APongPlayerState::SetPlayerIndex(uint32 Index)
{
	PlayerIndex = Index;
}

void APongPlayerState::GetLifetimeReplicatedProps(TArray<FLifetimeProperty>& OutLifetimeProps) const
{
	Super::GetLifetimeReplicatedProps(OutLifetimeProps);
	
	DOREPLIFETIME(APongPlayerState, PlayerIndex);
}

void APongPlayerState::UpdateScore(float NewScore)
{
	SetScore(NewScore);
	OnRep_Score();
}

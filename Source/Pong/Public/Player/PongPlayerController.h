#pragma once

#include "CoreMinimal.h"
#include "GameFramework/PlayerController.h"
#include "PongPlayerController.generated.h"

UCLASS()
class PONG_API APongPlayerController : public APlayerController
{
	GENERATED_BODY()

public:
	virtual void AutoManageActiveCameraTarget(AActor* SuggestedTarget) override;

	static class APongHUD* GetLocalHUD(const UWorld* InWorld);
};

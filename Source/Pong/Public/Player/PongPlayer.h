#pragma once

#include "CoreMinimal.h"
#include "GameFramework/Pawn.h"
#include "PongPlayer.generated.h"

UCLASS()
class PONG_API APongPlayer : public APawn
{
	GENERATED_UCLASS_BODY()

	UPROPERTY(VisibleDefaultsOnly, BlueprintReadOnly, Category = "Collision")
	class UBoxComponent* Collision;

	UPROPERTY(VisibleDefaultsOnly, BlueprintReadOnly, Category = "Collision")
	class UStaticMeshComponent* VisualMesh;

	UPROPERTY(EditDefaultsOnly, Category = "Input")
	float InputSensitivity;

protected:
	// Cached location boundaries 
	float MinYLocation;
	float MaxYLocation;
	
public:
	UFUNCTION(Server, Unreliable, WithValidation)
	void ServerAddVerticalInput(float Value);
	
	void AddVerticalInput(float Value);

	virtual void SetupPlayerInputComponent(class UInputComponent* PlayerInputComponent) override;
	
	virtual void OnConstruction(const FTransform& Transform) override;
};

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/PlayerState.h"
#include "PongPlayerState.generated.h"

UCLASS()
class PONG_API APongPlayerState : public APlayerState
{
	GENERATED_UCLASS_BODY()

protected:
	UPROPERTY(Replicated)
	uint32 PlayerIndex;

public:
	virtual void OnRep_Score() override;

	void SetPlayerIndex(uint32 Index);

	FORCEINLINE uint32 GetPlayerIndex() const;
	
	virtual void GetLifetimeReplicatedProps(TArray< FLifetimeProperty > & OutLifetimeProps) const override;

	void UpdateScore(float NewScore);
};

FORCEINLINE uint32 APongPlayerState::GetPlayerIndex() const
{
	return PlayerIndex;
}

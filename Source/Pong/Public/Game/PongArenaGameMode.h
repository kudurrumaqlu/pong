#pragma once

#include "CoreMinimal.h"
#include "GameFramework/GameModeBase.h"
#include "PongArenaGameMode.generated.h"

UCLASS()
class PONG_API APongArenaGameMode : public AGameModeBase
{
	GENERATED_UCLASS_BODY()
	
	UPROPERTY(EditDefaultsOnly, BlueprintReadOnly, Category = "Pong")
	int32 MaxScore;

	UPROPERTY(EditDefaultsOnly, BlueprintReadOnly, Category = "Pong")
	TSubclassOf<class APongBall> BallClass;

	UPROPERTY(EditDefaultsOnly, BlueprintReadOnly, Category = "Pong")
	float SpawnDelay;

	UPROPERTY(EditDefaultsOnly, BlueprintReadOnly, Category = "Pong")
	float MaxInitialBallAngle;

	UPROPERTY(EditDefaultsOnly, BlueprintReadOnly, Category = "Pong")
	float MatchRestartDelay;

	UPROPERTY(EditDefaultsOnly, Category=HUD)
	TSubclassOf<UUserWidget> ServerWaitingWidgetClass;

protected:
	UPROPERTY()
	TArray<class APlayerStart*> PlayerStarts;
	
	UPROPERTY()
	UUserWidget* ServerWaitingWidget;

	uint8 LastWinnerIndex;

	FTimerHandle BallSpawnTimer;

	FTimerHandle RestartTimerHandle;

public:
	virtual void BeginPlay() override;
	
	virtual AActor* FindPlayerStart_Implementation(AController* Player, const FString& IncomingName) override;

	virtual void StartPlay() override;

	virtual void HandleStartingNewPlayer_Implementation(APlayerController* NewPlayer) override;

	void EndMatch(uint8 WinnerIndex);

	UFUNCTION(BlueprintCallable, Category = "Pong")
	void StartNewMatch();

	void HandleFall(AActor* BallActor, uint8 PlayerIndex);
	
	void InitiateBallSpawn();
	
	void SpawnBall();

	FVector GetInitialDirection() const;
};

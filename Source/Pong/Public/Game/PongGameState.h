#pragma once

#include "CoreMinimal.h"
#include "GameFramework/GameState.h"
#include "PongGameState.generated.h"

UCLASS()
class PONG_API APongGameState : public AGameState
{
	GENERATED_BODY()
	
protected:
	UPROPERTY(Replicated, BlueprintReadOnly, VisibleInstanceOnly, Category = GameState)
	uint8 WinnerIndex;

	UPROPERTY(EditDefaultsOnly, BlueprintReadOnly, Category = "Sound")
	class USoundBase* FallSound;

public:
	void EndMatch(uint32 WinnerIndex);
	
	virtual void HandleMatchHasStarted() override;

	virtual void HandleMatchHasEnded() override;

	virtual void GetLifetimeReplicatedProps(TArray< FLifetimeProperty > &OutLifetimeProps) const override;

	UFUNCTION(Unreliable, NetMulticast)
	void MulticastNotifyFall();
};

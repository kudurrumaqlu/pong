#pragma once

#include "CoreMinimal.h"
#include "GameFramework/WorldSettings.h"
#include "PongWorldSettings.generated.h"

UCLASS()
class PONG_API APongWorldSettings : public AWorldSettings
{
	GENERATED_BODY()

public:
	UPROPERTY(EditDefaultsOnly, BlueprintReadOnly, Category = "Pong Level Settings")
	AActor* BallSpawn;
	
	UPROPERTY(EditDefaultsOnly, BlueprintReadOnly, Category = "Pong Level Settings")
	AActor* Player1Spawn;

	UPROPERTY(EditDefaultsOnly, BlueprintReadOnly, Category = "Pong Level Settings")
	AActor* Player2Spawn;

	UPROPERTY(EditDefaultsOnly, BlueprintReadOnly, Category = "Pong Level Settings")
	float LevelHeight = 250.f;
};

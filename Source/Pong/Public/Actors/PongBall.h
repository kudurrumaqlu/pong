#pragma once

#include "CoreMinimal.h"
#include "GameFramework/Actor.h"
#include "PongBall.generated.h"

UCLASS()
class PONG_API APongBall : public AActor
{
	GENERATED_UCLASS_BODY()
	
	UPROPERTY(VisibleDefaultsOnly, BlueprintReadOnly, Category = "Ball")
	class UStaticMeshComponent* Mesh;

	UPROPERTY(EditDefaultsOnly, BlueprintReadOnly, Category = "Ball")
	float InitialVelocity;

	UPROPERTY(EditDefaultsOnly, BlueprintReadOnly, Category = "Ball")
	float PerHitVelocityIncrease;
	
	UPROPERTY(EditDefaultsOnly, BlueprintReadOnly, Category = "Ball")
	float MaxPadReflectionAngle;

	UPROPERTY(EditDefaultsOnly, BlueprintReadOnly, Category = "Sound")
	class USoundBase* PadHitSound;

protected:
	uint32 HitCounter;

public:
	virtual void NotifyHit(class UPrimitiveComponent* MyComp, AActor* Other, class UPrimitiveComponent* OtherComp,
	                       bool bSelfMoved, FVector HitLocation, FVector HitNormal, FVector NormalImpulse,
	                       const FHitResult& Hit) override;
	virtual void BeginPlay() override;

	FORCEINLINE float GetInitialVelocity() const;

	UFUNCTION(NetMulticast, Unreliable)
	void MulticastPlayHitEffect();
};

FORCEINLINE float APongBall::GetInitialVelocity() const
{
	return InitialVelocity;
}

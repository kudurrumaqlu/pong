#pragma once

#include "CoreMinimal.h"
#include "GameFramework/Actor.h"
#include "PongFallArea.generated.h"

UCLASS()
class PONG_API APongFallArea : public AActor
{
	GENERATED_UCLASS_BODY()

	UPROPERTY(EditAnywhere, BlueprintReadOnly, Category = "Game Rules")
	uint8 PlayerIndex;

	UPROPERTY(VisibleAnywhere, BlueprintReadOnly, Category = "Collision")
	class UBoxComponent* Collision;

	virtual void NotifyActorBeginOverlap(AActor* OtherActor) override;
};

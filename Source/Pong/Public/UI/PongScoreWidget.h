#pragma once

#include "CoreMinimal.h"
#include "Blueprint/UserWidget.h"
#include "PongScoreWidget.generated.h"

UCLASS()
class PONG_API UPongScoreWidget : public UUserWidget
{
	GENERATED_BODY()

public:
	UFUNCTION(BlueprintImplementableEvent)
	void ScoreUpdated(int32 PlayerId, int32 Score);
};

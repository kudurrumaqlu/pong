#pragma once

#include "CoreMinimal.h"
#include "Blueprint/UserWidget.h"
#include "PongInfoWidget.generated.h"

UCLASS()
class PONG_API UPongInfoWidget : public UUserWidget
{
	GENERATED_BODY()

public:
	UFUNCTION(BlueprintImplementableEvent)
	void UpdateStatusText(const FText& Text);
};

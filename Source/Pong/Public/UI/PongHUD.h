#pragma once

#include "CoreMinimal.h"
#include "GameFramework/HUD.h"
#include "PongHUD.generated.h"

UCLASS()
class PONG_API APongHUD : public AHUD
{
	GENERATED_BODY()

public:
	UPROPERTY(EditDefaultsOnly, Category = "UI")
	TSubclassOf<class UPongScoreWidget> ScoreWidgetClass;

	UPROPERTY(EditDefaultsOnly, Category = "UI")
	TSubclassOf<class UPongInfoWidget> InfoWidgetClass;

protected:
	UPROPERTY()
	class UPongScoreWidget* ScoreWidget;
	
	UPROPERTY()
	class UPongInfoWidget* InfoWidget;

public:
	void UpdateScore(uint32 PlayerId, uint32 Score) const;

	void DisplayMatchResult(FText ResultMessage) const;

	void HideMatchResult() const;
	
	virtual void BeginPlay() override;
};
